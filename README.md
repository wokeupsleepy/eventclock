# EventClock

########
My Notes
########
The ClockComponent contains the ModalWindowComponent and the MessageBoardComponent.
The ClockComponent gets Event data from the EventCalendarService and the ClockService.
The ClockService controls the "game time" (represented by the currentTime Date property), it contains controls to pause time and jump time forward.
The EventCalendarService provides Event data and CRUD operations on them. It holds a "HeadEventList", which represents the list of Events that can be triggered.
Whenever an Event has "started", it opens the ModalWindowComponent and pauses the currentTime Date property on the ClockService.
The User must then make an action on the ModalWindowComponent, at which point the Event is "consumed" and is then removed from the "HeadEventList". Events can have "previousEvent" and "nextEvent" pointers. If an Event is consumed and it has a "nextEvent" pointer, the "nextEvent" is added the "HeadEventList" after the Event itself is consumed.

########
UNDER INVESTIGATION
########
So basically, you play a staffer in the Trump White House. You belong to one of the Principals. Basically, they are your Boss. Your Boss has a certain Loyalty level. Completely tasks for them generates Debt, which is a special form of currency used to for certain tasks.

You have certain wants and desires. Money and Influence (some nebulous concept, basically earned by backstabbing and "looking like you're accomplishing things", like helping to pass legislation and such) are abstracts. Trust and Suspicion (opposite of Trust, obviously) is per Principal. Trump and Pence are always Principals. Cabinet Members are temporarily Principals. Each successive Investigation is its own Principal. Principals have Minions. You don't know who the Minions are. Minions can belong to multiple Principals.

The Player has to save Secrets overheard from meetings and stuff. These Secrets can be passed out to NewsOutlets and also to a select number of Principals. If a Minion is known, then the Player can also pass to the Minion. NOTE THAT THE MINION MAY PASS INFORMATION TO ALL OF THEIR PRINCIPALS. NewsOutlets are ways of disseminating information that changes the levels of Trust/Suspicion. 

End Game:
Once the Investigations really gets going, the Player has to have a list of Secrets. These Secrets have to be saved in specific HidingSpots. If the Player attracts too much attention from any one of the Investigations, they may get a subpoena, a search warrant, or a no-knock search warrant (depending on the level of Suspicion).
	If they find the Secrets, and if some of them are incriminating, at this point, the Player can choose to help the Investigation or refuse. Either way, game over.
	OR, if the Player gets enough Trust with one of the Investigations (without raising the Suspicion of any one of the Principals, especially POTUS), the Player can choose to pass Secrets to them. Enough Secrets might bring down POTUS, or the Investigation may ask the Player to get more Secrets.
	OR, the Player may choose to use the Secrets to aid one of the Principals in their goals.

The Player can sabotage or help Principals and/or Minions.

So basically main goals are the following:
Manage Trust/Suspicion, Money, Influence with all, manage Debt with your Boss
Stay above a certain level of Suspicion with any one Principal, if Suspicion stays too high for too long, then game over
There is a lower threshold for YOUR Principal (i.e. your boss), if Suspicion stays too high for too long, then game over
EndGame: Endure the Investigation, Aid the Investigation, Aid the Principal

Go back to inauguration, catalogue a series of events, and make the Player make decisions for those events.

########
Angular Boilerplate
########

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Development server

Run `ng serve --open` for a dev server that auto-opens to browser. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## GAME NOTES

UNDER INVESTIGATION (4/24/2018)

So basically, you play a staffer in the Trump White House. You have certain wants and desires. Money and Influence (some nebulous concept, basically earned by backstabbing and "looking like you're accomplishing things", like helping to pass legislation and such) are abstracts. Trust and Suspicion (opposite of Trust, obviously) is with per Principal. Trump and Pence are always Principals. Cabinet Members are temporarily Principals. Each successive Investigation is its own Principal. Principals have Minions. You don't know who the Minions are. Minions can belong to multiple Principals. 

The Player has to save Secrets overheard from meetings and stuff. These Secrets can be passed out to NewsOutlets and also to a select number of Principals. If a Minion is known, then the Player can also pass to the Minion. NOTE THAT THE MINION MAY PASS INFORMATION TO ALL OF THEIR PRINCIPALS. NewsOutlets are ways of disseminating information that changes the levels of Trust/Suspicion. 

Once the Investigations really gets going, the Player has to have a list of Secrets. These Secrets have to be saved in specific HidingSpots. If the Player attracts too much attention from any one of the Investigations, they may get a subpoena, a search warrant, or a no-knock search warrant (depending on the level of Suspicion). If they find the Secrets, and if some of them are incriminating, at this point, the Player can choose to help the Investigation or refuse. Either way, game over. OR, if the Player gets enough Trust with one of the Investigations (without raising the Suspicion of any one of the Principals, especially POTUS), the Player can choose to pass Secrets to them. Enough Secrets might bring down POTUS, or the Investigation may ask the Player to get more Secrets.

The Player can sabotage or help Principals and/or Minions.

Go back to inauguration, catalogue a series of events, and make the Player make decisions for those events.

