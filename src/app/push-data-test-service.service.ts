import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PushDataTestServiceService {
  private data: Observable<number>;
  private values: Array<number> = [];
  private pushDataStarted: boolean = false;

  private startingIntValue: number = 100;

  constructor() {
  }

  pushData(): Observable<number> {
    console.log("pushData method was STARTED");

    this.data = new Observable(observer => {
      let intToDisplay: number = this.startingIntValue;
      let tempValuesArray = this.values;

      if (tempValuesArray.length > 0) {
        intToDisplay = tempValuesArray[tempValuesArray.length - 1] + 2;
      }

      tempValuesArray.push(intToDisplay);

      console.log(tempValuesArray);
      observer.next(intToDisplay);

      console.log("Data was pushed from PushDataTestService");
    });

    return this.data;
  }

}