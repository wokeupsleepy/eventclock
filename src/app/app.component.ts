import { Component, AfterViewInit } from '@angular/core';

@Component({
  // selector: 'app-root',
  selector: 'body',
  // host:     {'[class.someClass]':'someField'}
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'See this here?';

  //NOTE: Use someField with the "host" property in @Component above to dynamically set the class
  someField: boolean = false;
  // alternatively to the host parameter in `@Component`
  // @HostBinding('class.someClass') someField: bool = false;

  ngAfterViewInit() {
    this.someField = true; // set class `someClass` on `<body>`
  }
}
