import { Injectable, OnInit } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable, Subscription } from 'rxjs/Rx';
import { ObservableInput } from 'rxjs/Observable';

import { Event } from './event-calendar.service';
import { STARTINGCHARACTERPROPERTIES } from './objects-on-init';
import { Console } from '@angular/core/src/console';

export class Character {
  id: number;
  name: string;
  happiness: number;
  properties: Array<CharacterProperty>;
  actionAttention: Array<CharacterAction>;
}

export class CharacterPropertyStub {
  id: number;
  propertyName: string;
}

export class CharacterProperty extends CharacterPropertyStub {
  intensity: number;
}

//NOTE: This is an Action that takes up all the Character's Attention
export class CharacterAction {
  ActionText: string;
  ActionDuration: number;
}

@Injectable()
export class CharactersService {
  private playerCharacter: Character;
  private playerCount: number = 0;
  private characterPropertyList: Array<CharacterProperty> = [];

  constructor() {
    this.createCharacterPropertyListOnInit();
    this.createPlayerCharacter();
  }

  private createCharacterPropertyListOnInit(): void {
    let startingCharacterPropertyStubs = STARTINGCHARACTERPROPERTIES;

    for (let i = 0; i < startingCharacterPropertyStubs.length; i++) {
      if (this.characterPropertyList[startingCharacterPropertyStubs[i].propertyName]) {
        let foundProperty = this.characterPropertyList[startingCharacterPropertyStubs[i].propertyName];

        console.log("Property already exists for: " + foundProperty.propertyName + ", id: " + foundProperty.id);
      }
      else {
        let newCharacterProperty = new CharacterProperty();

        //NOTE: Move values from the CharacterPropertyStub to the CharacterProperty
        newCharacterProperty.id = startingCharacterPropertyStubs[i].id;
        newCharacterProperty.propertyName = startingCharacterPropertyStubs[i].propertyName;
        newCharacterProperty.intensity = 0;

        this.characterPropertyList[startingCharacterPropertyStubs[i].propertyName] = newCharacterProperty;
      }
    }

  }

  private createPlayerCharacter() {
    this.playerCharacter = new Character;
    this.playerCharacter.id = this.playerCount;
    this.playerCharacter.name = "Wade Garrison";
    this.playerCharacter.happiness = 0;
    this.playerCharacter.properties = [];
    this.playerCharacter.actionAttention = [];

    try {
      let sadProperty: CharacterProperty = this.getCharacterPropertyByPropertyName("sad");
      let depressedProperty: CharacterProperty = this.getCharacterPropertyByPropertyName("depressed");
      let energeticProperty: CharacterProperty = this.getCharacterPropertyByPropertyName("energetic");

      //NOTE: Need explicit functions for adding a CharacterProperty OR incrementing "intensity" of existing CharacterProperty
      this.addCharacterPropertyToCharacter(sadProperty, this.playerCharacter);
      this.addCharacterPropertyToCharacter(sadProperty, this.playerCharacter);
      this.addCharacterPropertyToCharacter(depressedProperty, this.playerCharacter);
    }
    catch (error) {
      console.log(error);
    }

    this.playerCount++;
  }

  public getPlayerCharacter(): Character {
    return this.playerCharacter;
  }

  private getCharacterPropertyByPropertyName(propertyName: string): CharacterProperty {
    let foundProperty = this.characterPropertyList[propertyName];

    if (!foundProperty) {
      throw "CharactersService.getCharacterPropertyObjectByPropertyName(): CharacterProperty not found!";
    }

    return foundProperty;
  }

  private addCharacterPropertyToCharacter(newProperty: CharacterProperty, character: Character): void {
    let existingPropertyOnCharacter = character.properties.find(function (property) {
      return property.id === newProperty.id;
    });

    if (existingPropertyOnCharacter) {
      existingPropertyOnCharacter.intensity++;
    }
    else {
      character.properties.push(newProperty);
    }
  }

  private removeCharacterPropertyFromCharacter(removedProperty: CharacterProperty, character: Character): void {
    for (let propertyCounter = 0; propertyCounter < character.properties.length; propertyCounter++) {
      if (character.properties[propertyCounter].id === removedProperty.id) {
        if (character.properties[propertyCounter].intensity > 0) {
          character.properties[propertyCounter].intensity--;
          break;
        }
        else {
          character.properties.splice(propertyCounter, 1);
          break;
        }
      }
    }
  }

  public updateAffectedCharacterFromEvent(event: Event, isExpired?: boolean): boolean {
    let finalHappinessChange = event.happiness;

    //NOTE: If the User clicked one of the buttons from the Event, add the corresponding buttonEffect from the Event
    if (event.buttonClickedValue) {
      let index = event.buttons.indexOf(event.buttonClickedValue);
      //NOTE: Need to find the corresponding values
      // console.log("See the value of button clicked from modal in CharactersService: ");
      // console.log(event.buttonClickedValue);
      // console.log(event.buttons);
      // console.log(event.buttonEffects);
      // console.log(index);

      if (index > -1) {
        let correspondingButtonEffect = event.buttonEffects[index];

        console.log(correspondingButtonEffect);

        let buttonEffectArray = correspondingButtonEffect.split(';');

        for (let i = 0; i < buttonEffectArray.length; i++) {
          event.eventEffects.push(buttonEffectArray[i]);
        }
      }
    }

    let eventsBeingUsedDeterminedByIsExpired = event.eventEffects;

    if (isExpired) {
      eventsBeingUsedDeterminedByIsExpired = event.expireEffects;
    }

    if (event.affectedCharacter &&
      eventsBeingUsedDeterminedByIsExpired &&
      eventsBeingUsedDeterminedByIsExpired.length > 0 &&
      event.affectedCharacter.properties.length > 0
    ) {
      let character = event.affectedCharacter;

      //NOTE: Due to this "for loop", there is a natural "precedence" of effects wherein the first effect occurs first
      for (let effectCounter = 0; effectCounter < eventsBeingUsedDeterminedByIsExpired.length; effectCounter++) {
        let parsedEffect = eventsBeingUsedDeterminedByIsExpired[effectCounter].split(':');
        let effectType = parsedEffect[0];
        let watchedProperty = parsedEffect[1];
        let effect = parsedEffect[2];

        // console.log("parsed Event effects from: " + event.eventText);
        // //NOTE: This should always be here, indicates the type of thing that happens to the affectedCharacter
        // //(Event's happiness value is changed, add CharacterProperty, delete CharacterProperty, etc.)
        // console.log(effectType);
        // //NOTE: Some effects only take place if the Character has the "watchedProperty"
        // console.log(watchedProperty);
        // //NOTE: This is the "value" of the effect, defines the CharacterProperty that is added or deleted
        // console.log(effect);
        // console.log("------------");

        switch (effectType) {
          case "modifyEventHappiness":
            /*
            NOTE: Calculation:
            event.baseHappiness + "happinessVariability change" (min 0, max happinessVariability) = event.happiness
            (event.happiness + effect) * happinessChangeMultiplier = finalHappinessChange
            */
            for (let propertyCheckCounter = 0; propertyCheckCounter < character.properties.length; propertyCheckCounter++) {
              let propertyNameFromCharacter = character.properties[propertyCheckCounter].propertyName;

              if (watchedProperty === propertyNameFromCharacter) {
                if (effect.startsWith("=")) {
                  let explicitHappinessChangeFromProperty = effect.replace("=", "");
                  finalHappinessChange = parseInt(explicitHappinessChangeFromProperty);
                }
                else {
                  let happinessChangeFromProperty = parseInt(effect);
                  finalHappinessChange = finalHappinessChange + happinessChangeFromProperty;
                }

                if (character.properties[propertyCheckCounter].intensity > 0) {
                  let happinessChangeMultiplier = character.properties[propertyCheckCounter].intensity + 1;
                  finalHappinessChange = finalHappinessChange * happinessChangeMultiplier;
                }

                break;
              }
            }
            break;
          case "addProperty":
            if (watchedProperty === "") {
              try {
                let newPropertyToAdd = this.getCharacterPropertyByPropertyName(effect);
                this.addCharacterPropertyToCharacter(newPropertyToAdd, character);
              } catch (error) {
                console.log(error);
              }
            }
            break;
          case "deleteProperty":
            if (watchedProperty === "") {
              try {
                let propertyToDelete = this.getCharacterPropertyByPropertyName(effect);
                this.removeCharacterPropertyFromCharacter(propertyToDelete, character);
              } catch (error) {
                console.log(error);
              }
            }
            break;
          case "addAction":
            if (watchedProperty === "") {
              try {
                let newAction = new CharacterAction();
                this.addActionToCharacter(newAction, character);

              } catch (error) {
                console.log(error);
              }
            }
            break;
          default:
            console.log("CharactersService.updateAffectedCharacterFromEvent(): Invalid effectType:"
              + effectType + " eventId: " + event.id);
        }
      }
    }

    if (event.affectedCharacter) {
      event.affectedCharacter.happiness = event.affectedCharacter.happiness + finalHappinessChange;
      return true;
    }
    else {
      return false;
    }
  }

  private addActionToCharacter(action: CharacterAction, character: Character): boolean {
    if (character.actionAttention.length > 0) {
      throw "This Character: " + character.name + " already has an Action!";
      // return false;
    }
    else {
      character.actionAttention.push(action);

      return true;
    }
  }


}
