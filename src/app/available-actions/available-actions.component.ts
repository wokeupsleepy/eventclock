import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Observable, Subscription, Observer } from 'rxjs/Rx';

import { Event } from '../event-calendar.service';

@Component({
  selector: 'app-available-actions',
  templateUrl: './available-actions.component.html',
  styleUrls: ['./available-actions.component.css']
})
export class AvailableActionsComponent implements OnInit {
  @Input('eventListInput') eventListFromClock: Array<Event>;
  @Output() sendCompletedEventToClockComponent = new EventEmitter<Event>();

  constructor() { }

  ngOnInit() {
  }

  startEvent(startedEvent?: Event): void {
    this.sendCompletedEventToClockComponent.emit(startedEvent);
  }

}
