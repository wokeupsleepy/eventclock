import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailableActionsComponent } from './available-actions.component';

describe('AvailableActionsComponent', () => {
  let component: AvailableActionsComponent;
  let fixture: ComponentFixture<AvailableActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailableActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailableActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
