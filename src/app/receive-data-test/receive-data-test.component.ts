import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { PushDataTestServiceService } from '../push-data-test-service.service';
import { ClockService } from '../clock.service';

@Component({
  selector: 'app-receive-data-test',
  templateUrl: './receive-data-test.component.html',
  styleUrls: ['./receive-data-test.component.css']
})
export class ReceiveDataTestComponent implements OnInit {
  private data: Observable<number>;
  private values: Array<number> = [];
  private status: string;

  private retrievedClockTime: Date;

  constructor(private pushDataTestServiceService: PushDataTestServiceService, private clockService: ClockService) { }

  ngOnInit() {
    this.status = "started on start";

    this.data = this.pushDataTestServiceService.pushData();

    // let subscription = this.data.forEach(
    //   value => this.values.push(value)
    // );

  }

  init() {
    this.status = "button clicked from Receive Data Test Component";

    // this.data = this.pushDataTestServiceService.pushData();

    let subscription = this.data.forEach(
      value => this.values.push(value)
    );
    
  }

  getClockTime() {
    this.clockService.getTime()
      .subscribe(currentTime => this.retrievedClockTime = currentTime);

    this.status = this.retrievedClockTime.toString();
  }

}
