import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription, Observer } from 'rxjs/Rx';

import { Event } from '../event-calendar.service';

@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.css']
})
export class MessageBoardComponent implements OnInit {
  messages = [];
  currentTime: Date;

  @Input('eventList') eventList: Array<Event>;
  @Input('startedEventList') startedEventList: Array<Event>;

  // addMessage(): void{
  //   this.getClock();
  //   this.messages.push("Another message added at: " + this.currentTime);

  //   //NOTE: The "this.masterName" gets its value from the "valueFromParent" property on the ClockComponent
  //   //This can be seen from the clock.component.html
  //   this.addExplicit("see this haaha " + this.masterName);

  //   console.log("event list from Message Board");
  //   console.log(this.eventList);
  // }

  addNewMessage(newMessage: string): void {
    this.messages.push(newMessage);
  }

  constructor(
  ) { }


  ngOnInit() {
  }

}
