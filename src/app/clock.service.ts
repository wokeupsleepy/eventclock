import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable, Subscription } from 'rxjs/Rx';
import { checkAndUpdateTextInline } from '@angular/core/src/view/text';
import { ObservableInput } from 'rxjs/Observable';

import { EventCalendarService, Event } from './event-calendar.service';

@Injectable()
export class ClockService {
  private currentTime: Date;
  private clockIsActive: boolean = true;
  private delayTimer = 1000;
  private timerInteval = 1000;

  minuteDifferenceFromCurrent = 0;
  startedEvents: Array<Event> = [];
  expiredEvents: Array<Event> = [];

  getTime(): Observable<Date> {
    return of(this.currentTime);
  }

  getClockIsActiveStatus(): Observable<boolean> {
    return of(this.clockIsActive);
  }

  private initializeTime(): void {
    if (!this.currentTime) {
      this.currentTime = new Date("02-01-2018 20:00:00");

      //NOTE: These next 2 lines are just to show that an initial time can be set
      // this.currentTime.setMinutes(15);
      // this.currentTime.setHours(21);
      // this.currentTime.setSeconds(0);
    }
  }

  private advanceTime(): void {
    let setTime = new Date(this.currentTime);
    setTime.setMinutes(setTime.getMinutes() + 1);
    // setTime.setSeconds(setTime.getSeconds() + 15);

    if (this.minuteDifferenceFromCurrent != 0) {
      setTime.setMinutes(setTime.getMinutes() + this.minuteDifferenceFromCurrent);
      this.minuteDifferenceFromCurrent = 0;
    }

    this.currentTime = setTime;
  }

  addMinuteDifference(minuteInput): void {
    this.minuteDifferenceFromCurrent = this.minuteDifferenceFromCurrent + minuteInput;
  }

  toggleClock(): void {
    this.clockIsActive = !this.clockIsActive;
  }

  pauseClock(): void {
    this.clockIsActive = false;
  }

  startClock(): void {
    this.clockIsActive = true;
  }

  getStartedEvents(): Observable<Array<Event>> {
    return of(this.startedEvents);
  }

  consumeEvent(eventToConsume: Event, expired?: boolean): void {
    //NOTE: This removes eventToConsume from the EventCalendarService
    // this.eventCalendarService.consumeEvent(eventToConsume, this.currentTime);

    if (expired) {
      this.eventCalendarService.expireEvent(eventToConsume, this.currentTime);
    }
    else {
      this.eventCalendarService.consumeEvent(eventToConsume, this.currentTime);
    }

    //NOTE: This removes eventToConsume from the ClockService
    let counter;
    for (counter = 0; counter < this.startedEvents.length; counter++) {
      let currentCountedEvent = this.startedEvents[counter];

      if (currentCountedEvent.id === eventToConsume.id) {
        break;
      }
    }

    this.startedEvents.splice(counter, 1);
  }

  //NOTE: This is needed for setting the time and getting started Events, which the ClockComponent will eventually pick up
  private timer = Observable.timer(this.delayTimer, this.timerInteval);
  private sub = this.timer.subscribe(t => this.tickerFunction(t));

  tickerFunction(tick) {
    if (this.clockIsActive) {
      this.clockIsActive = false;
      this.advanceTime();

      this.eventCalendarService.getStartedEvents(this.currentTime)
        .subscribe(events => this.startedEvents = events);

      this.eventCalendarService.getExpiredEvents(this.currentTime)
        .subscribe(events => this.expiredEvents = events);

      if (this.expiredEvents.length > 0) {
        this.consumeEvent(this.expiredEvents[0], true);
      }

      this.clockIsActive = true;
    }
  }

  constructor(private eventCalendarService: EventCalendarService) {
    this.initializeTime();
    this.eventCalendarService.createStartingEventsOnInit(this.currentTime);
  }

}
