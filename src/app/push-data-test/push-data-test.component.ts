import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { PushDataTestServiceService } from '../push-data-test-service.service';

@Component({
  selector: 'app-push-data-test',
  templateUrl: './push-data-test.component.html',
  styleUrls: ['./push-data-test.component.css']
})
export class PushDataTestComponent implements OnInit {
  private data: Observable<number>;
  private values: Array<number> = [];
  private anyErrors: boolean;
  private finished: boolean;
  private status: string;

  constructor(private pushDataTestServiceService: PushDataTestServiceService) {
  }

  ngOnInit() {
    // this.status = "button clicked";

    this.data = this.pushDataTestServiceService.pushData();

    // let subscription = this.data.forEach(
    //   value => this.values.push(value)
    // );

  }

  init() {

    // alert("see this?");

    //NOTE: This doesn't do anything, you need to set up a subscription
    // this.pushDataTestServiceService.pushData();

    this.status = "button clicked from Push Data Test Component";

    // this.data = this.pushDataTestServiceService.pushData();

    let subscription = this.data.forEach(
      value => this.values.push(value)
    );
  }

}
