import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';

import { ClockService } from '../clock.service';
import { Event } from '../event-calendar.service';
import { CharactersService, Character } from '../characters.service';

import { ModalWindowComponent } from '../modal-window/modal-window.component';
import { MessageBoardComponent } from '../message-board/message-board.component';
import { AvailableActionsComponent } from '../available-actions/available-actions.component';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})

/*

NOTE: This is for managing the actual time of the game
and organizing the Events and state changes as time transitions.
Essentially, it centralizes "getting" data and passing it on to other data outputs (MessageBoardComponent, ModalWindowComponent).

*/

export class ClockComponent implements OnInit {
  currentTime: Date;
  isClassVisible: boolean;

  //NOTE: Try getting a "push" from the clock service as opposed to the current "pulls" using subscribe
  testObserver: Observer<boolean>;
  eventList: Array<Event>;
  startedEventList: Array<Event>;

  playerCharacter: Character;

  //NOTE: These are just dummy values passed from the ClockComponent to the ModalWindowComponent
  //as examples of data transfers from parent to child
  master = 'Dr. Master';
  passedEvent: Event;

  @ViewChild(ModalWindowComponent) modalWindow: ModalWindowComponent;
  @ViewChild(AvailableActionsComponent) availableActions: AvailableActionsComponent;

  constructor(
    private clockService: ClockService
    , private charactersService: CharactersService
  ) { }

  getClock(): void {
    this.clockService.getTime()
      .subscribe(currentTime => this.currentTime = currentTime);
  }

  getTimeDifference(): number {
    return this.clockService.minuteDifferenceFromCurrent;
  }

  addMinutes(): void {
    this.clockService.addMinuteDifference(15);
  }

  toggleClock(): void {
    this.clockService.toggleClock();
  }

  printPlayerCharacterToConsole(): void {
    if (this.playerCharacter) {
      console.log(this.playerCharacter);
    }
  }

  getStartedEventList(): void {
    this.clockService.getStartedEvents()
      .subscribe(eventList => this.startedEventList = eventList);
  }

  consumeEventFromModal(eventFromModalWindow: Event) {
    if(eventFromModalWindow){
      this.clockService.consumeEvent(eventFromModalWindow);
    }
    this.getStartedEventList();
    this.clockService.startClock();
  }

  getClickedEventFromAvailableActions(eventFromAvailableActions: Event) {
    //NOTE: This is just for passing some dummy values
    if (!this.passedEvent.eventText) {
      this.passedEvent.eventText = "see this here from clock? this should only appear when the User first clicks an Action button.";
    }
    else {
      this.passedEvent.eventText = "only changes on Actions, this should be seen every time after the first.";
    }

    this.modalWindow.getDataFromClockAndOpenModal(eventFromAvailableActions);
  }


  /*
    NOTE: The tickerFunc() is the execution of a single time unit.
    It does the following:
      1) Advance time
      2) Check if any existing/running events have started, send to Message Board Component


      3) Resolve event triggers
      4) Check if any events should be added to the EventCalendar
      5) Do anything else needed before moving to next tick
    */

  //NOTE: Unlike the ClockService, ClockComponent never stops ticking. This is so we only have 1 thing to start/stop
  private tickerFunc(tick) {
    //NOTE: Event list is being retrieved here
    this.getStartedEventList();

    this.getClock();
  }

  private tickerFuncTimer;
  private tickerFuncSubscription: Subscription;

  ngOnInit() {
    //NOTE: This is needed for getting the time from the ClockService the first time
    this.getClock();

    //NOTE: This is just for a dummy value passed to ModalWindowComponent as example
    this.passedEvent = new Event;

    //NOTE: This is for repeatedly getting the time from ClockService repeatedly using a timer
    this.tickerFuncTimer = Observable.timer(1000, 1000);
    this.tickerFuncSubscription = this.tickerFuncTimer.subscribe(t => this.tickerFunc(t));

    //NOTE: This is for getting the player character the first time
    this.playerCharacter = this.charactersService.getPlayerCharacter();

    //NOTE: 2-6-2018, I'm not sure what I was using testObserver for, but I think it's for making the clock appear red?
    //See how it's tied to isClassVisible? I think that's related somehow
    this.isClassVisible = false;
  }

}
