import { EventStub } from './event-calendar.service';
import { CharacterPropertyStub } from './characters.service';

export const STARTINGEVENTS: EventStub[] = [
  {
    eventText: "I need to make dinner, but I'm a little tired",
    startTime: new Date("02-01-2018 20:03:00"),
    eventEffects: [
      // "modifyEventHappiness:sad:-3",
      // "modifyEventHappiness:content:+1"
      "addProperty::sad",
      "addProperty::depressed"
    ],
    buttons: [
      "make instant ramen noodles",
      "make mac and cheese"
    ],
    buttonEffects: [
      // "modifyEventHappiness:sad:-3",
      "addProperty::content;addProperty::sad",
      "addAction::makeMacAndCheese;addAction::makeMacAndCheese"
    ],
    expireEffects: [
      "addProperty::depressed",
      "addAction::makeMacAndCheese"
    ],
    baseHappiness: 1,
    // happinessVariability: 2,
    happinessVariability: 0,
    variabilityIsNegative: true
  }
  ,
  {
    eventText: "eat dinner, dinner is bad, adds depression, see this later in the evening",
    startTime: new Date("02-01-2018 20:10:00"),
    eventEffects: [
      "addProperty::depressed"
    ],
    buttons: null,
    buttonEffects: null,
    expireEffects: [
      "addProperty::sad",
      "addProperty::depressed",
      "addProperty::energetic"
    ],
    baseHappiness: 1,
    // happinessVariability: 3,
    happinessVariability: 0,
    variabilityIsNegative: true
  }

  // {
  //   eventText: "SEE FIRST: I need to make dinner, but I'm a little tired",
  //   startTime: new Date("02-01-2018 20:03:00"),
  //   eventEffects: [
  //     "modifyEventHappiness:sad:-3",
  //     "modifyEventHappiness:content:+1"
  //   ],
  //   buttons: [
  //     "make instant ramen noodles",
  //     "make mac and cheese"
  //   ],
  //   buttonEffects: [
  //     "modifyEventHappiness:sad:-2",
  //     "addProperty::content"
  //   ],
  //   expireEffects: [
  //     "addProperty::depressed",
  //     "addAction::makeMacAndCheese"
  //   ],
  //   startingHappiness: 1,
  //   happinessVariability: 2,
  //   variabilityIsNegative: true
  // }
  // ,
  // {
  //   eventText: "eat dinner, dinner is bad, adds depression, see this later in the evening",
  //   startTime: new Date("02-01-2018 20:10:00"),
  //   eventEffects: [
  //     "addProperty::depressed"
  //   ],
  //   buttons: null,
  //   buttonEffects: null,
  //   expireEffects: [
  //     "addProperty::sad",
  //     "addProperty::depressed",
  //     "addProperty::energetic"
  //   ],
  //   startingHappiness: 1,
  //   happinessVariability: 3,
  //   variabilityIsNegative: true
  // }
  // ,
  // {
  //   eventText: "can't find anything on TV, feel worse",
  //   startTime: new Date("02-01-2018 20:13:00"),
  //   eventEffects: [
  //     "modifyEventHappiness:depressed:=-10"
  //   ],
  //   buttons: null,
  //   buttonEffects: null,
  //   startingHappiness: 1,
  //   happinessVariability: 2,
  //   variabilityIsNegative: true
  // },
  // {
  //   eventText: "get call from friend, removes depression and sad, adds content",
  //   startTime: new Date("02-01-2018 20:16:00"),
  //   eventEffects: [
  //     "deleteProperty::depressed",
  //     "deleteProperty::sad",
  //     "addProperty::content"
  //   ],
  //   buttons: null,
  //   buttonEffects: null,
  //   startingHappiness: 1,
  //   happinessVariability: 3,
  //   variabilityIsNegative: false
  // },
  // {
  //   eventText: "finish making dinner, see this later before EATING DINNER and after STARTING MAKING DINNER",
  //   startTime: new Date("02-01-2018 20:08:00"),
  //   eventEffects: null,
  //   buttons: null,
  //   buttonEffects: null,
  //   startingHappiness: 1,
  //   happinessVariability: 1,
  //   variabilityIsNegative: false
  // },
  // {
  //   eventText: "received phone call, see this at the same time as STARTING MAKING DINNER",
  //   startTime: new Date("02-01-2018 20:05:00"),
  //   eventEffects: null,
  //   buttons: null,
  //   buttonEffects: null,
  //   startingHappiness: 0,
  //   happinessVariability: 2,
  //   variabilityIsNegative: false
  // }
];

export const STARTINGCHARACTERPROPERTIES: CharacterPropertyStub[] = [
  {
    id: 0,
    propertyName: "sad"
  },
  {
    id: 1,
    propertyName: "depressed"
  },
  {
    id: 2,
    propertyName: "content"
  },
  {
    id: 3,
    propertyName: "energetic"
  }
];