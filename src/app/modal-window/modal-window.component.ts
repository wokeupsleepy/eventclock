import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Observable, Subscription, Observer } from 'rxjs/Rx';

import { Event } from '../event-calendar.service';
import { ClockService } from '../clock.service';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.css']
})
export class ModalWindowComponent implements OnInit {
  private modalId;
  private clockIsActive: boolean;
  private manuallyOpenedModal: boolean = false;

  private currentStartedEvent: Event;

  // @Input('startedEventList') startedEventList: Array<Event>;
  // @Input('eventListInput') startedEventList: Array<Event>;
  // @Input('startedEvent') startedEvent: Event;

  @Input('masterValue') masterName: string;
  @Input('passedEventFromClock') eventFromClock: Event;

  @Output() sendCompletedEventToClockComponent = new EventEmitter<Event>();

  constructor(
    // private eventCalendarService: EventCalendarService,
    private clockService: ClockService
  ) { }

  manuallyOpenModal(): void {
    this.manuallyOpenedModal = true;

    console.log(this.eventFromClock);
    console.log("---passed from input from ClockComponent");

    this.openModal();
  }

  getDataFromClockAndOpenModal(event: Event): void {
    // console.log("This was opened from the available actions component");
    // console.log(this.startedEventList);
    // console.log(event);
    // console.log("----------");

    this.openModal(event);
  }

  openModal(event?: Event): void {
    this.clockService.pauseClock();
    this.modalId.style.display = "block";

    // console.log(this.startedEvent);
    // console.log(this.startedEventList);

    // console.log(this.masterName);
    // console.log(this.eventFromClock);

    if (event) {
      this.currentStartedEvent = event;
    }
    else {
    }
  }

  closeModal(buttonClickedValue?: string): void {
    if (buttonClickedValue) {
      this.currentStartedEvent.buttonClickedValue = buttonClickedValue;
    }
    this.modalId.style.display = "none";
    this.sendCompletedEventToClockComponent.emit(this.currentStartedEvent);
    this.currentStartedEvent = null;
  }

  setControls(): void {
    // Get the modal
    this.modalId = document.getElementById('myModal');

    // When the user clicks anywhere outside of the modal, close it
    // window.onclick = function (event) {
    //   if (event.target == modal) {
    //     modal.style.display = "none";
    //   }
    // }
  }

  ngOnInit() {
    this.setControls();
  }

  //NOTE: This was just experiments with keyboard events
  // values = '';
  // onKey(event: KeyboardEvent) { // without type info
  //   this.values += (<HTMLInputElement>event.target).value + ' | ';
  // }

}