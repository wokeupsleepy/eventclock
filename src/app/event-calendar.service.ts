/*
NOTE:
The EventCalendarService should only be called from the ClockService.
Components should not directly call the EventCalendarService, but instead call the ClockService,
which in turn routes to functions on the EventCalendarService.
*/

import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable, Subscription } from 'rxjs/Rx';
import { ObservableInput } from 'rxjs/Observable';

import { Character, CharactersService } from './characters.service';

import { STARTINGEVENTS } from './objects-on-init';
import { RangeObservable } from 'rxjs/observable/RangeObservable';

export class EventStub {
  // id: number;
  eventText: string;
  startTime: Date;
  // expireTime: Date;
  // createTime: Date;
  eventEffects: Array<string>;
  expireEffects: Array<string>;
  baseHappiness: number;
  happinessVariability: number;
  variabilityIsNegative: boolean;

  //NOTE: When the User clicks a button, it appends the corresponding buttonEffect
  //to the Event's "affectedByProperties" before consuming the Event
  buttons: Array<string>;
  buttonEffects: Array<string>;
}

export class Event extends EventStub {
  id: number;
  // eventText: string;
  // startTime: Date;
  expireTime: Date;
  createTime: Date;
  history: string;
  happiness: number;
  affectedCharacter: Character;

  previousEvent: Event;
  nextEvent: Event;

  buttonClickedValue: string;
}

@Injectable()
export class EventCalendarService {
  private headEventList: Array<Event> = [];
  private consumedEventList: Array<Event> = [];
  private eventCounter: number = 0;

  constructor(
    private charactersService: CharactersService
  ) {
  }

  getStartedEvents(time: Date): Observable<Array<Event>> {
    let startedEvents: Array<Event> = [];

    //NOTE: Assuming that the this.headEventList is sorted with event.startTime descending, pick up the first Event and check it
    if (this.headEventList && this.headEventList.length > 0) {
      for (let i = 0; i < this.headEventList.length; i++) {
        let currentEvent = this.headEventList[i];

        if (currentEvent.startTime <= time) {
          startedEvents.push(currentEvent);
        }
      }
    }

    return of(startedEvents);
  }

  getExpiredEvents(time: Date): Observable<Array<Event>> {
    let expiredEvents: Array<Event> = [];

    //NOTE: Assuming that the this.headEventList is sorted with event.startTime descending, pick up the first Event and check it
    if (this.headEventList && this.headEventList.length > 0) {
      for (let i = 0; i < this.headEventList.length; i++) {
        let currentEvent = this.headEventList[i];

        if (currentEvent.expireTime <= time) {
          expiredEvents.push(currentEvent);
        }
      }
    }

    return of(expiredEvents);
  }

  //////////////////////////////////////////////////////////

  consumeEvent(tempEvent: Event, consumedAtTime: Date): void {
    if (tempEvent) {
      //NOTE: Make the effect on the affectedCharacter
      this.charactersService.updateAffectedCharacterFromEvent(tempEvent);

      this.updateEventHistory(tempEvent, "Event consumed at: " + consumedAtTime);
      if (tempEvent.buttonClickedValue) {
        this.updateEventHistory(tempEvent, "clicked button: " + tempEvent.buttonClickedValue);
      }

      //NOTE: Handle nextEvent and previousEvent pointers
      if (tempEvent.nextEvent !== null) {
        let tempNextEvent = tempEvent.nextEvent;

        tempNextEvent.previousEvent = null;
        tempEvent.nextEvent = null;

        this.updateEventHistory(tempNextEvent, "Event triggered by: " + tempEvent.id + " at " + consumedAtTime);
        this.consumedEventList.push(tempEvent);

        this.headEventList.push(tempNextEvent);
        this.headEventList.sort(this.compareByStartTime);

        tempNextEvent = null;
      }
      else {
        this.consumedEventList.push(tempEvent);
      }

      this.findAndRemoveHeadEvent(tempEvent);

      console.log("These are the consumed Events");
      console.log(this.consumedEventList);
      console.log("------------");
    }
  }

  expireEvent(tempEvent: Event, expiredAtTime: Date): void {
    console.log("eventCalendarService expireEvent");
    if (tempEvent) {
      //NOTE TODO: Make the effect on the affectedCharacter for expiredEvent
      this.charactersService.updateAffectedCharacterFromEvent(tempEvent, true);

      this.updateEventHistory(tempEvent, "Event EXPIRED at: " + expiredAtTime);
      this.consumedEventList.push(tempEvent);
      this.findAndRemoveHeadEvent(tempEvent);

      console.log("These are the EXPIRED Events");
      console.log(this.consumedEventList);
      console.log("------------");

      // console.log("These are the remaining Events");
      // console.log(this.headEventList);
      // console.log("------------");
    }
  }

  private findAndRemoveHeadEvent(event: Event): void {
    let counter;
    for (counter = 0; counter < this.headEventList.length; counter++) {
      let currentCountedEvent = this.headEventList[counter];

      if (currentCountedEvent.id === event.id) {
        break;
      }
    }

    this.headEventList.splice(counter, 1);
  }

  private updateEventHistory(eventToUpdate: Event, updateText: string): void {
    eventToUpdate.history = eventToUpdate.history + updateText + ";";
  }

  //NOTE: This is called from the ClockService because we need to know what the starting game time is
  createStartingEventsOnInit(startGameTime: Date): void {
    let startingEventStubs = STARTINGEVENTS;
    // console.log(startingEventStubs);

    for (let i = 0; i < startingEventStubs.length; i++) {
      let newEvent = new Event();

      // newEvent.id = startingEventStubs[i].id;
      newEvent.id = this.eventCounter;

      //NOTE: These are all the values from the startingEventStub that needs to be moved to the Event
      newEvent.eventText = startingEventStubs[i].eventText;
      newEvent.startTime = startingEventStubs[i].startTime;
      newEvent.eventEffects = startingEventStubs[i].eventEffects;
      newEvent.expireEffects = startingEventStubs[i].expireEffects;
      newEvent.buttons = startingEventStubs[i].buttons;
      newEvent.buttonEffects = startingEventStubs[i].buttonEffects;
      newEvent.baseHappiness = startingEventStubs[i].baseHappiness;
      newEvent.happinessVariability = startingEventStubs[i].happinessVariability;
      newEvent.variabilityIsNegative = startingEventStubs[i].variabilityIsNegative;

      newEvent.createTime = startGameTime;

      let tempExpireDate = new Date(newEvent.startTime);
      tempExpireDate.setMinutes(tempExpireDate.getMinutes() + this.generateRandomInt(30));
      newEvent.expireTime = tempExpireDate;

      newEvent.history = "";

      // newEvent.happiness = this.generateRandomInt(5);
      newEvent.happiness = newEvent.baseHappiness +
        this.generateRandomInt(newEvent.happinessVariability, newEvent.variabilityIsNegative);

      // newEvent.happiness = this.generateRandomInt(0);
      newEvent.affectedCharacter = this.charactersService.getPlayerCharacter();

      newEvent.previousEvent = null;
      newEvent.nextEvent = null;

      this.eventCounter++;
      this.addHeadEvent(newEvent);

      //NOTE: Make 2 linked Events per startingEventStub, I know this works so I'm commenting out for now
      // this.createTestLinkedEvent(newEvent);
      // this.createTestLinkedEvent(newEvent);
    }

    console.log("total number of events from createStartingEventsOnInit(): " + this.eventCounter);
    console.log("List of Events: ");
    console.log(this.headEventList);
    console.log("--------------");
  }

  //NOTE: This creates a dummy Event and adds it to the end of the first Head Event
  //NOTE: the createTime on these test Linked Events won't be the logic used later on
  //because it needs to be based off of this.currentTime on ClockService
  private createTestLinkedEvent(headEvent: Event): void {
    //NOTE: Need to traverse to the end of the linked list
    let tailEvent = headEvent;

    while (tailEvent.nextEvent !== null) {
      tailEvent = tailEvent.nextEvent;
    }

    let newEvent = new Event;

    newEvent.id = this.eventCounter;
    this.eventCounter++;

    newEvent.eventText = "see this LINKED text here? Id: " + newEvent.id;
    newEvent.createTime = new Date();

    let tempStartDate = new Date(tailEvent.startTime);
    tempStartDate.setMinutes(tailEvent.startTime.getMinutes() + this.generateRandomInt(20));
    newEvent.startTime = tempStartDate;

    let tempExpireDate = new Date(tempStartDate);
    tempExpireDate.setMinutes(tempExpireDate.getMinutes() + this.generateRandomInt(30));
    newEvent.expireTime = tempExpireDate;

    newEvent.history = "";
    newEvent.happiness = this.generateRandomInt(5, true);
    newEvent.affectedCharacter = this.charactersService.getPlayerCharacter();

    newEvent.previousEvent = null;
    newEvent.nextEvent = null;

    //NOTE: This adds a dummy Event to the end of the first Head Event
    let addedLinkedEventSuccess = this.addLinkedEvent(newEvent, headEvent);

    // console.log("linked event added?: " + addedLinkedEventSuccess);
    // console.log(this.headEventList);
  }

  private generateRandomInt(maxIntValue: number, isNegative?: boolean) {
    //NOTE: Need to set some sort of default value in case of invalid value
    if (maxIntValue < 0) {
      maxIntValue = 20;
    }

    let randomValue = Math.floor(Math.random() * (maxIntValue + 1));

    if (isNegative) {
      randomValue = randomValue * -1;
    }
    return randomValue;
  }

  private addHeadEvent(newEvent: Event): boolean {
    if (newEvent.nextEvent === null && newEvent.previousEvent === null) {
      this.headEventList.push(newEvent);
      this.headEventList.sort(this.compareByStartTime);

      return true;
    }
    else {
      return false;
    }
  }

  private addLinkedEvent(newLinkedEvent: Event, headEvent: Event): boolean {
    if (headEvent.previousEvent === null) {
      let currentEvent = headEvent;

      while (currentEvent.nextEvent !== null) {
        currentEvent = currentEvent.nextEvent;
      }

      currentEvent.nextEvent = newLinkedEvent;
      newLinkedEvent.previousEvent = currentEvent;

      return true;
    }
    else {
      return false;
    }
  }

  private compareByStartTime(a, b): number {
    if (a.startTime < b.startTime) {
      return -1;
    }
    if (a.startTime > b.startTime) {
      return 1;
    }
    return 0;
  }

}