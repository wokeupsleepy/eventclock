import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ClockComponent } from './clock/clock.component';
import { MessageBoardComponent } from './message-board/message-board.component';
import { ModalWindowComponent } from './modal-window/modal-window.component';

import {ClockService} from './clock.service';
import {PushDataTestServiceService} from './push-data-test-service.service';
import {EventCalendarService} from './event-calendar.service';
import {CharactersService} from './characters.service';

import { PushDataTestComponent } from './push-data-test/push-data-test.component';
import { ReceiveDataTestComponent } from './receive-data-test/receive-data-test.component';
import { AddEventCalendarComponent } from './add-event-calendar/add-event-calendar.component';
import { AvailableActionsComponent } from './available-actions/available-actions.component';

@NgModule({
  declarations: [
    AppComponent,
    ClockComponent,
    MessageBoardComponent,
    ModalWindowComponent,
    PushDataTestComponent,
    ReceiveDataTestComponent,
    AddEventCalendarComponent,
    AvailableActionsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ClockService, PushDataTestServiceService, EventCalendarService, CharactersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
