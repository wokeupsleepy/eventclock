import { TestBed, inject } from '@angular/core/testing';

import { EventCalendarService } from './event-calendar.service';
import { CharactersService } from './characters.service';

describe('EventCalendarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventCalendarService, CharactersService]
    });
  });

  it('should be created', inject([EventCalendarService], (service: EventCalendarService) => {
    expect(service).toBeTruthy();
  }));
});
